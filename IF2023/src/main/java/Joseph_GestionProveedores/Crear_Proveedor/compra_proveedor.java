package Joseph_GestionProveedores.Crear_Proveedor;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import funcionesPrincipales.FuncionesProperties;
import funcionesPrincipales.Libreria;

public class compra_proveedor extends Libreria {
	
	FuncionesProperties fp = new FuncionesProperties();
	int data=11, xpath=6;
	WebDriver driver2;

	public compra_proveedor(WebDriver driver2) {
		super(driver2);
		// TODO Auto-generated constructor stub
	}
	
	public void compra_proveedor() {
		try {
			Thread.sleep(4000);
			click(By.xpath(fp.leerProperties("BtnCompra", xpath)));
			Thread.sleep(4000); 
			click(By.xpath(fp.leerProperties("BtnNuevoCompra", xpath)));
			
			Thread.sleep(3000);
			SendKeysTecla("FC_Proveedor", xpath, "FC_Proveedor", data, Keys.ENTER);
			Thread.sleep(1000);
			sendKeys(fp.leerProperties("FC_RefProveedor", data), By.xpath(fp.leerProperties("FC_RefProveedor", xpath)));
			//ClearText(By.xpath(fp.leerProperties("FC_Divisa", xpath)));
			Thread.sleep(1000);
			//SendKeysTecla("FC_Divisa", xpath, "FC_Divisa", data, Keys.ENTER);
			
			click(By.xpath(fp.leerProperties("FC_FechaPedidoDesplegar", xpath)));
			Thread.sleep(1000);
			click(By.xpath(fp.leerProperties("FC_FechaMes", xpath)));
			Thread.sleep(1000);
			click(By.xpath(fp.leerProperties("FC_FechaDia", xpath)));
			Thread.sleep(1000);
			click(By.xpath(fp.leerProperties("FC_FechaConfirmar", xpath)));
			Thread.sleep(1000);
			
			/* Autogenerado por el sistema
			click(By.xpath(fp.leerProperties("FC_FechaEntregaDesplegar", xpath)));
			Thread.sleep(1000);
			click(By.xpath(fp.leerProperties("FC_FechaMes", xpath)));
			Thread.sleep(1000);
			click(By.xpath(fp.leerProperties("FC_FechaDia", xpath)));
			Thread.sleep(1000);
			click(By.xpath(fp.leerProperties("FC_FechaConfirmar", xpath)));
			Thread.sleep(2000); */
			
			//click(By.xpath(fp.leerProperties("FC_PedirConfirmacionChk", xpath))); el sistema lo activa por defecto	
			ClearAndSetText(fp.leerProperties("FC_PedirConfirmacionDias", data), By.xpath(fp.leerProperties("FC_PedirConfirmacionDias", xpath)));
			
			click(By.xpath(fp.leerProperties("FC2_AgregarProd", xpath)));
			Thread.sleep(1000);
			SendKeysTecla("FC2_IngresarProd", xpath, "FC2_IngresarProd", data, Keys.ENTER);
			Thread.sleep(1000);
			ClearText(By.xpath(fp.leerProperties("FC2_CantidadProd", xpath)));
			SendKeysTecla("FC2_CantidadProd", xpath, "FC2_CantidadProd", data, Keys.TAB);
			Thread.sleep(1000);
			ClearText(By.xpath(fp.leerProperties("FC2_PrecioProd", xpath)));
			SendKeysTecla("FC2_PrecioProd", xpath, "FC2_PrecioProd", data, Keys.TAB);
			Thread.sleep(1000);
			SendKeysTecla("FC2_ImpuestoProd", xpath, "FC2_ImpuestoProd", data, Keys.ENTER);
			Thread.sleep(1000);
			
			click(By.xpath(fp.leerProperties("FC3_BtnOtraInfo", xpath)));
			Thread.sleep(1000);
			ClearAndSetText(fp.leerProperties("FC3_Comprador", data), By.xpath(fp.leerProperties("FC3_Comprador", xpath)));
			Thread.sleep(1000);
			SendKeysTecla("FC3_Incoterm", xpath, "FC3_Incoterm", data, Keys.ENTER);
			Thread.sleep(1000);
			sendKeys(fp.leerProperties("FC3_IncotermUb", data), By.xpath(fp.leerProperties("FC3_IncotermUb", xpath)));
			ClearText(By.xpath(fp.leerProperties("FC3_TerminosPago", xpath)));
			Thread.sleep(1000);
			SendKeysTecla("FC3_TerminosPago", xpath, "FC3_TerminosPago", data, Keys.ENTER);
			Thread.sleep(1000);
			SendKeysTecla("FC3_PosicionFiscal", xpath, "FC3_PosicionFiscal", data, Keys.ENTER);
			Thread.sleep(2000);
			
			click(By.xpath(fp.leerProperties("FC_CofirmarPedido", xpath)));
			Thread.sleep(2000);
			click(By.xpath(fp.leerProperties("FC_SalirCompraForm", xpath)));
			Thread.sleep(2000);
			click(By.xpath(fp.leerProperties("BtnRegresar", xpath)));
			Thread.sleep(3000);
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
