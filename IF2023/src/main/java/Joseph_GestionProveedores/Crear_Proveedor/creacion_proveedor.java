package Joseph_GestionProveedores.Crear_Proveedor;

import java.io.File;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.service.DriverCommandExecutor;

import com.lowagie.text.html.simpleparser.Img;

import funcionesPrincipales.FuncionesProperties;
import funcionesPrincipales.Libreria;

public class creacion_proveedor extends Libreria{
	
	FuncionesProperties fp = new FuncionesProperties();
	int data=11, xpath=6;
	WebDriver driver2;
	File imgRuta = new File ("C:\\Users\\JosephVG\\Downloads\\prueba.jpeg");

	public creacion_proveedor(WebDriver driver2) {
		super(driver2);
		this.driver2 = driver2;
		// TODO Auto-generated constructor stub
	}
	
	public void creacion_proveedor () {
		try {
			Thread.sleep(3000);
			click(By.xpath(fp.leerProperties("BtnCompra", xpath)));
			Thread.sleep(2000);
			click(By.xpath(fp.leerProperties("BtnPedidos", xpath)));
			Thread.sleep(1000);
			click(By.xpath(fp.leerProperties("OpProveedores", xpath)));
			Thread.sleep(1000);
			click(By.xpath(fp.leerProperties("BtnNuevoProveedor", xpath)));
			
			Thread.sleep(2000);
			SendKeysTecla("F_Nombre", xpath, "F_Nombre", data, Keys.TAB);
			Thread.sleep(1000);
			sendKeys(fp.leerProperties("F_Calle", data), By.xpath(fp.leerProperties("F_Calle", xpath)));
			sendKeys(fp.leerProperties("F_Calle2", data), By.xpath(fp.leerProperties("F_Calle2", xpath)));
			sendKeys(fp.leerProperties("F_Ciudad", data), By.xpath(fp.leerProperties("F_Ciudad", xpath)));
			SendKeysTecla("F_Pais", xpath, "F_Pais", data, Keys.TAB);
			Thread.sleep(1000);
			SendKeysTecla("F_Estado", xpath, "F_Estado", data, Keys.TAB);
			Thread.sleep(1000);
			sendKeys(fp.leerProperties("F_CP", data), By.xpath(fp.leerProperties("F_CP", xpath)));
			sendKeys(fp.leerProperties("F_NIT", data), By.xpath(fp.leerProperties("F_NIT", xpath)));
			sendKeys(fp.leerProperties("F_Tel", data), By.xpath(fp.leerProperties("F_Tel", xpath)));
			sendKeys(fp.leerProperties("F_Movl", data), By.xpath(fp.leerProperties("F_Movl", xpath)));
			sendKeys(fp.leerProperties("F_Email", data), By.xpath(fp.leerProperties("F_Email", xpath)));
			sendKeys(fp.leerProperties("F_Web", data), By.xpath(fp.leerProperties("F_Web", xpath)));
			SendKeysTecla("F_Catg", xpath, "F_Catg", data, Keys.TAB);
			Thread.sleep(1000);
			
			Imagen("BtnEditar", "logo_prueba.png", xpath);
			Thread.sleep(1000);

			click(By.xpath(fp.leerProperties("Pesta�a_VentaCompra", xpath)));
			Thread.sleep(1000);
			
			SendKeysTecla("F2_Comercial", xpath, "F2_Comercial", data, Keys.ENTER);
			SendKeysTecla("F2_TerminosPago", xpath, "F2_TerminosPago", data, Keys.TAB);
			Thread.sleep(1000);
			SendKeysTecla("F2_Tarifa", xpath, "F2_Tarifa", data, Keys.TAB);
			Thread.sleep(1000);
			SendKeysTecla("F2_MetodoEntrega", xpath, "F2_MetodoEntrega", data, Keys.TAB);
			Thread.sleep(1000);
			
			SendKeysTecla("F2_Comprador", xpath, "F2_Comprador", data, Keys.TAB);
			Thread.sleep(1000);
			SendKeysTecla("F2_TermioPago_Comprador", xpath, "F2_TermioPago_Comprador", data, Keys.TAB);
			Thread.sleep(1000);
			click(By.xpath(fp.leerProperties("F2_Recordatorio_Checkbox", xpath)));
			Thread.sleep(1000);
			sendKeys(fp.leerProperties("F2_Recordatorio_Dias", data), By.xpath(fp.leerProperties("F2_Recordatorio_Dias", xpath)));
			SendKeysTecla("F2_MonedaProveedor", xpath, "F2_MonedaProveedor", data, Keys.TAB);
			Thread.sleep(1000);
			
			sendKeys(fp.leerProperties("F2_EmpresaID", data), By.xpath(fp.leerProperties("F2_EmpresaID", xpath)));
			sendKeys(fp.leerProperties("F2_Referencia", data), By.xpath(fp.leerProperties("F2_Referencia", xpath)));
			SendKeysTecla("F2_SitioWeb", xpath, "F2_SitioWeb", data, Keys.ENTER);
			SendKeysTecla("F2_Sector", xpath, "F2_Sector", data, Keys.TAB);	
			
			Thread.sleep(2000);
			click(By.xpath(fp.leerProperties("SalirForm", xpath)));	
			
			Thread.sleep(2000);
			click(By.xpath(fp.leerProperties("BtnRegresar", xpath)));	
			Thread.sleep(3000);
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
