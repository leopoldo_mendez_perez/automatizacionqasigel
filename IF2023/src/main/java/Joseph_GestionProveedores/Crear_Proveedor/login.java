package Joseph_GestionProveedores.Crear_Proveedor;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import funcionesPrincipales.FuncionesProperties;
import funcionesPrincipales.Libreria;

public class login extends Libreria{
	
	FuncionesProperties fp = new FuncionesProperties();
	int data=11, xpath=6;
	
	public login(WebDriver driver2) {
		super(driver2);
		// TODO Auto-generated constructor stub
	}
	public void login() {
		sendKeys(fp.leerProperties("user", data), By.xpath(fp.leerProperties("Usuario", xpath)));
		sendKeys(fp.leerProperties("password", data), By.xpath(fp.leerProperties("Contrasenia", xpath)));
		click(By.xpath(fp.leerProperties("BtnIniciarSesion", xpath)));
	}
}
