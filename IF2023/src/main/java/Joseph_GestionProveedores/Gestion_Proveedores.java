package Joseph_GestionProveedores;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Joseph_GestionProveedores.Crear_Proveedor.login;
import Joseph_GestionProveedores.Crear_Proveedor.compra_proveedor;
import Joseph_GestionProveedores.Crear_Proveedor.creacion_proveedor;
import funcionesPrincipales.FuncionesProperties;
import funcionesPrincipales.Libreria;


public class Gestion_Proveedores {

	private WebDriver driver;
	private WebDriverWait wait;
	private Actions action;
	int data=11, xpath=6;
	private Libreria properties = new Libreria(driver);
	FuncionesProperties fp = new FuncionesProperties();

	@BeforeTest
	public void setUp() {

		driver=  properties.ChromeDriverConnection();
		// driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get(fp.leerProperties("ODO", 2));
		action = new Actions(driver);
	}

	//Login
	@Test(priority = 1)
	public void Login() throws InterruptedException {
		login l=new login (driver);
		l.login();
		
	}
	
	//CrearProveedor
	@Test(priority = 2)
	public void Proveedor() throws InterruptedException {
		creacion_proveedor cp=new creacion_proveedor (driver);
		cp.creacion_proveedor();
	}
	
	//CrearCompra
	@Test(priority = 3)
	public void Compra() throws InterruptedException {
		compra_proveedor pr=new compra_proveedor (driver);
		pr.compra_proveedor();
	}
	
		@AfterTest
	public void tearDown() {
		driver.quit();
	}

}
