package LeopoldoPruebasAutomatizadasCRM;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import funcionesPrincipales.FuncionesProperties;
import funcionesPrincipales.Libreria;


public class CreacionClienteCRM {

	private WebDriver driver;
	private WebDriverWait wait;
	private Actions action;
	private Libreria properties = new Libreria(driver);
	 FuncionesProperties fp = new FuncionesProperties();

	@BeforeTest
	public void setUp() {
		driver=  properties.ChromeDriverConnection();
		// driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get(fp.leerProperties("ODO", 2));
		action = new Actions(driver);
	}
   
	// CambioFacturacionExistenteCostaRica
	@Test(priority = 1)
	public void login() throws InterruptedException {
	properties.sendKeys(fp.leerProperties("user", 1),By.xpath(fp.leerProperties("Usuario", 3)));
	properties.sendKeys(fp.leerProperties("password", 1),By.xpath(fp.leerProperties("Contrasenia", 3)));

	}
	
	@Test(priority = 2)
	public void crm() throws InterruptedException {
		
	}
	
	@AfterTest
	public void tearDown() {
		//driver.quit();
	}

}
