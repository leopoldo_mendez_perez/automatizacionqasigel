package BeltherRegistroProducto;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import BeltherRegistroProducto.CrearProducto.informacionproducto;
import BeltherRegistroProducto.CrearProducto.inventario2;
import BeltherRegistroProducto.CrearProducto.login;
import BeltherRegistroProducto.CrearProducto.paginaweb;
import funcionesPrincipales.FuncionesProperties;
import funcionesPrincipales.Libreria;


public class RegistroProducto {

	private WebDriver driver;
	private WebDriverWait wait;
	private Actions action;
	private Libreria properties = new Libreria(driver);
	FuncionesProperties fp = new FuncionesProperties();
	int data = 13, xpath=8;

	@BeforeTest
	public void setUp() {

		driver=  properties.ChromeDriverConnection();
		// driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get(fp.leerProperties("ODO", 2));
		action = new Actions(driver);
	}

	@Test(priority = 1)
	public void Login() throws InterruptedException {
		login l=new login(driver);
		l.login();
	}

	@Test(priority = 2)
	public void Inventario() throws InterruptedException{
		inventario2 i= new inventario2(driver);
		i.inventario();
	}
	
	@Test(priority = 3)
	public void Informacion() throws InterruptedException{
		informacionproducto i2= new informacionproducto(driver);
		i2.informacionproducto();
	}
	
	@Test(priority = 4)
	public void Pagina() throws InterruptedException{
		paginaweb p = new paginaweb(driver);
		p.paginaweb();
	}
	
	@AfterTest
	public void tearDown() {
		driver.quit();
	}

}
