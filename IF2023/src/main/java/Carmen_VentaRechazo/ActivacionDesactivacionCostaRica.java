package Carmen_VentaRechazo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.model.Log;


//import Carmen_VentaRechazo.Carmen_VentaRechazo.F_Rectivicativa;
import Carmen_VentaRechazo.Carmen_VentaRechazo.Ingreso_Datos;
import Carmen_VentaRechazo.Carmen_VentaRechazo.Mensaje;
import Carmen_VentaRechazo.Carmen_VentaRechazo.Nueva_factura;
import Carmen_VentaRechazo.Carmen_VentaRechazo.VentaRechazo;
import funcionesPrincipales.FuncionesProperties;
import funcionesPrincipales.Libreria;


public class ActivacionDesactivacionCostaRica {

	private WebDriver driver;
	private WebDriverWait wait;
	private Actions action;
	private Libreria properties = new Libreria(driver);
	FuncionesProperties fp = new FuncionesProperties();

	@BeforeTest
	public void setUp() {
		driver=  properties.ChromeDriverConnection();
		// driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get(fp.leerProperties("ODO", 2));
		action = new Actions(driver);
	}
   
	// CambioFacturacionExistenteCostaRica
	@Test(priority = 1)
	public void login() throws InterruptedException {
	Carmen_VentaRechazo.Carmen_VentaRechazo.login l=new Carmen_VentaRechazo.Carmen_VentaRechazo.login(driver);
	l.login();
	}
	
	@Test(priority = 2)
	public void crm() throws InterruptedException {
		VentaRechazo c=new VentaRechazo(driver);
		c.ingcontabilidad();
	}
	
	@Test(priority = 3)
	public void btnNF() throws InterruptedException {
		Nueva_factura c=new Nueva_factura(driver);
		c.btnNF();
	}
	
	@Test(priority = 4)
	public void Cliente() throws InterruptedException {
		Ingreso_Datos c=new Ingreso_Datos(driver);
		c.Cliente();
	}
	
	@Test(priority = 5)
	public void MensajeFa() throws InterruptedException {
		Mensaje c=new Mensaje(driver);
		c.FacturaMensaje();
	}
	


	@AfterTest
	public void tearDown() {
		driver.quit();
	}

}