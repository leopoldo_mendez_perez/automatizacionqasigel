package SandorLeiva_pruebasautomatizadas.nuevo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import funcionesPrincipales.FuncionesProperties;
import funcionesPrincipales.Libreria;


public class Login extends Libreria{
	FuncionesProperties fp = new FuncionesProperties();

	public Login(WebDriver driver2) {
		super(driver2);
		// TODO Auto-generated constructor stub
	}

	public void login() throws InterruptedException {

		sendKeys(fp.leerProperties("user", 9),By.xpath(fp.leerProperties("Usuario", 4)));
		sendKeys(fp.leerProperties("password", 9),By.xpath(fp.leerProperties("Contrasenia", 4)));
		click(By.xpath(fp.leerProperties("BtnIniciarSesion", 4)));
		
	}

	
}
