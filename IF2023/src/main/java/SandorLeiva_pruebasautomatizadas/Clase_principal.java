package SandorLeiva_pruebasautomatizadas;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import SandorLeiva_pruebasautomatizadas.nuevo.Login;
import funcionesPrincipales.FuncionesProperties;
import funcionesPrincipales.Libreria;


public class Clase_principal {

	private WebDriver driver;
	private WebDriverWait wait;
	private Actions action;
	private Libreria properties = new Libreria(driver);
	 FuncionesProperties fp = new FuncionesProperties();

	@BeforeTest
	public void setUp() {

		driver=  properties.ChromeDriverConnection();
		// driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get(fp.leerProperties("ODO", 2));
		action = new Actions(driver);
	}

	// CambioFacturacionExistenteCostaRica
	@Test(priority = 1)
	public void login() throws InterruptedException {
		
		Login l=new Login(driver);
		l.login();
	}
	
	
	@Test(priority = 2)
	public void Venta_nueva() throws InterruptedException {
		SandorLeiva_pruebasautomatizadas.nuevo.Venta_nueva V=new SandorLeiva_pruebasautomatizadas.nuevo.Venta_nueva(driver);
		V.Venta_nueva();
	}
	
	
	
	@AfterTest
	public void tearDown() {
		driver.quit();
	}

}
