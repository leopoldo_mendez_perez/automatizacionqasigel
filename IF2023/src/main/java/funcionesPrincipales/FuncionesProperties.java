package funcionesPrincipales;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Vector;

import javax.swing.JOptionPane;


public class FuncionesProperties {

	public String leerProperties(String propertie,int num) {
		Properties propiedades = new Properties();
		InputStream entrada = null;

		propiedades.getProperty(propertie);
		try {
			entrada = new FileInputStream(direccionProperties(num));
			propiedades.load(entrada);
			//propiedades.getProperty(propertie);
			entrada.close();
			return propiedades.getProperty(propertie);
		}catch(FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}catch(IOException e) {
			e.printStackTrace();
			return null;
		}
	}


	private String direccionProperties(int i) {
		switch(i) {
		
		case 1:
			return "./src/main/java/properties/propertiesData.properties";
		case 2:	
			return "./src/main/java/properties/propertiesSystem.properties";
		case 3:
			return "./src/main/java/properties/propertiesXpath.properties";
		case 4:
			return "./src/main/java/properties/propertiesXpathSandor.properties";
		case 5:
			return "./src/main/java/properties/propertiesXpathAnthony.properties";
		case 6:
			return "./src/main/java/properties/propertiesXpathJoseph.properties";
		case 7:
			return "./src/main/java/properties/propertiesXpathCarmen.properties";
		case 8:
			return "./src/main/java/properties/propertiesXpathBelther.properties";
		case 9:
			return "./src/main/java/properties/propertiesDataSandor.properties";	
		case 10:
			return "./src/main/java/properties/propertiesDataAnthony.properties";
		case 11:
			return "./src/main/java/properties/propertiesDataJoseph.properties";
		case 12:
			return "./src/main/java/properties/propertiesDataCarmen.properties";
		case 13:
			return "./src/main/java/properties/propertiesDataBelther.properties";	
		
		
		}
		return null;
	}
	
}