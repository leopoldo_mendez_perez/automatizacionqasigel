package funcionesPrincipales;

import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;

import java.io.File;
import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Libreria {
	private WebDriver driver;
	private WebDriverWait wait;
	FuncionesProperties fp=new FuncionesProperties();
	public Libreria(WebDriver driver2) {
		// TODO Auto-generated constructor stub
		driver=driver2;
	}
	public void sendKeys(String inputText, By locator) {
		try {
			driver.findElement(locator).sendKeys(inputText);

		} catch (Exception e) {
         System.out.println("Input No encontrado: "+locator);
		}
	}
	public void click(By locator) {
		driver.findElement(locator).click();
	}
	public void ClearAndSetText(String text, By locator) {
		WebElement element = driver.findElement(locator);
		Actions navigator = new Actions(driver);
		navigator.click(element).sendKeys(Keys.END).keyDown(Keys.SHIFT).sendKeys(Keys.HOME).keyUp(Keys.SHIFT)
		.sendKeys(Keys.BACK_SPACE).sendKeys(text).sendKeys(Keys.ENTER).perform();
	}
	public void ClearText(By locator) {
		WebElement element = driver.findElement(locator);

		Actions navigator = new Actions(driver);
		navigator.click(element).sendKeys(Keys.END).keyDown(Keys.SHIFT).sendKeys(Keys.HOME).keyUp(Keys.SHIFT)
				.sendKeys(Keys.BACK_SPACE).sendKeys("").perform();

	}
	public boolean exist(String element) {
		boolean condicion;
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.MILLISECONDS);
		try {
			condicion = driver.findElements(By.xpath(element)).size() != 0;
		} catch (NoSuchElementException e) {
			condicion = false;
		}
		return condicion;
	}
	public void waitPresenceOfElementLocated(int seconds, By element) {
		wait = new WebDriverWait(driver, seconds);
		wait.until(presenceOfElementLocated(element));
		// driver.wait();
		// return (WebDriverWait) wait.until(presenceOfElementLocated(element));
	}
	public void waitPresenceOfElementLocatedClick(int seconds, By element) {
		wait = new WebDriverWait(driver, seconds);
		wait.until(presenceOfElementLocated(element)).click();
		// return (wait);
	}
	public By ByXpath(String localizador) {
		return By.xpath(localizador);
	}
	public WebDriver ChromeDriverConnection () {
		System.setProperty("webdriver.chrome.driver", fp.leerProperties("chromeDriver",2));
		driver = new ChromeDriver();
		return driver;
	}
	public void SendKeysTecla(String Localizador, int DireccionXpath, String Texto, int DireccionData, Keys tecla) {
		driver.findElement(By.xpath(fp.leerProperties(Localizador,DireccionXpath))).sendKeys(fp.leerProperties(Texto, DireccionData),tecla);;
	}
	public void Imagen(String Localizador, String NombreImagen,int xpath) {
		File img = new File("./src/main/java/img/"+NombreImagen);
		String ruta = img.getAbsolutePath();
		driver.findElement(By.xpath(fp.leerProperties(Localizador,xpath))).sendKeys(ruta);
	}
	public String getTitle() {
		return driver.getTitle();
	}

	public boolean existWhitParameter(String element, int a) throws InterruptedException {
		boolean condicion;
		driver.manage().timeouts().implicitlyWait(a, TimeUnit.MILLISECONDS);
		Thread.sleep(0);
		try {
			condicion = driver.findElements(By.xpath(element)).size() != 0;

		} catch (NoSuchElementException e) {
			condicion = false;
		}
		return condicion;

	}
}
