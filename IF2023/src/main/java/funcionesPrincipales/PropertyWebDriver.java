package funcionesPrincipales;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.Properties;

import javax.swing.JOptionPane;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;


public class PropertyWebDriver {
	private WebDriver driver;
Libreria fp=new Libreria(driver);
FuncionesProperties funpro=new FuncionesProperties();
	public PropertyWebDriver(WebDriver driver) {
		this.driver = driver;
	}

	public String leerProperties(String propertie,int pais) {
		Properties propiedades = new Properties();
		InputStream entrada = null;

		propiedades.getProperty(propertie);
		try {
			switch(pais) {
	          //ChromeDriver Panama
			case 1:
				entrada = new FileInputStream("./src/test/java/properties_Panama/system.properties");
            break;	
            //ChromeDriver CostaRica
            case 2:
				entrada = new FileInputStream("./src/test/java/properties/system.properties");
	            break;
			//ExplorerDriverConnection Honduras y CostaRieca
			case 3:
			entrada = new FileInputStream("./src/test/java/properties/system.properties");
            break;
			case 4:
				entrada = new FileInputStream("./src/test/java/properties_Guatemala/system.properties");
	            break;
		}
			propiedades.load(entrada);
			//propiedades.getProperty(propertie);
			return propiedades.getProperty(propertie);
		}catch(FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}catch(IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public WebDriver ChromeDriverConnectionPanama () {
		   //driver=fp.initCRMGuatemala(driver,action);
		System.setProperty("webdriver.chrome.driver", leerProperties("webDriver",1));
		// pass the debuggerAddress and pass the port along with host. Since I am running test on local so using localhost
		//opt.setExperimentalOption("debuggerAddress","localhost:9222 ");
		//driver=new ChromeDriver(opt);
		// pass ChromeOptions object to ChromeDriver constructor

		driver = new ChromeDriver();
		return driver;
	}
	public WebDriver ChromeDriverConnectionGT () {
	/*	ChromeOptions opt=new ChromeOptions();
			   //driver=fp.initCRMGuatemala(driver,action);
			System.setProperty("webdriver.chrome.driver", leerProperties("webDriver",1));
			System.out.println( "Configuracion crome Driver: "+leerProperties("webDriver",1));
			// pass the debuggerAddress and pass the port along with host. Since I am running test on local so using localhost
			opt.setExperimentalOption("debuggerAddress","localhost:9222 ");
			driver=new ChromeDriver(opt);
			// pass ChromeOptions object to ChromeDriver constructor

			fp.setAcrivacionDePruebas(true);
			driver = new ChromeDriver();*/

		System.setProperty("webdriver.chrome.driver", leerProperties("webDriver",4));
		driver = new ChromeDriver();
			return driver;
		}
	public WebDriver ChromeDriverNavegadoActual (int pais) {	 
	 	runChrome();
		System.setProperty("webdriver.chrome.driver",funpro.leerProperties("webDriver", pais));
		ChromeOptions options = new ChromeOptions();
		options.setExperimentalOption("debuggerAddress","127.0.0.1:1559");
		ChromeDriver driver = new ChromeDriver(options);
		return driver;
	}
	public WebDriver ChromeDriverConnection () {
		System.setProperty("webdriver.chrome.driver", leerProperties("webDriver",2));
		driver = new ChromeDriver();
		return driver;
	}


	public WebDriver ExplorerDriverConnection() {
		System.setProperty("webdriver.ie.driver", leerProperties("webDriverExplorer",3));
		driver = new InternetExplorerDriver();
		return driver;
	}
	public static void runChrome() {
		
		Process p = null;
		try {
			p = Runtime.getRuntime().exec("cmd.exe /K start C:\\Users\\QA\\AppData\\Local\\Google\\Chrome\\bat.lnk");
		    BufferedReader input = new BufferedReader (new InputStreamReader (p.getInputStream()));

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
